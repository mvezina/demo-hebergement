"use strict";

// const { populate } = require('../models/post');
// Récupère le modèle Post
const Post = require('../models/post');

// Utilise la méthode find() afin de récupérer tous les articles
// Retourne un Promesse
exports.getPosts = (req, res, next) => {
  Post.find()
  .then(posts => {
    res.json(posts);
  })
  .catch(err => {
    next(err);
  });
};

// Récupère un article grâce à son id
exports.getPost = (req, res, next) => {
  const postId = req.params.postId;
  Post.findById(postId)
  .then(post => {
    if (!post) {
      const error = new Error('L\'article n\'existe pas.');
      error.statusCode = 404;
      throw error;
    }
    res.json(post);
  })
  .catch(err => {
    next(err);
  });
};

// Enregistre un article dans la bd
exports.createPost = (req, res, next) => {
  const title = req.body.title;
  const content = req.body.content;

  // Crée un nouvel article avec les informations passées dans le body
  const post = new Post({
    title: title,
    content: content,
  });

  // Enregistre l'article dans la base de données
  // Utilisation de la méthode save() qui retourne une promesse
  post.save()
    .then(result => {
      res.status(200).json(post);
    })
    .catch(err => {
      next(err);
    });
};

// Enregistre un article modifié dans la bd
exports.updatePost = (req, res, next) => {
  const postId = req.body.postId;
  const title = req.body.title;
  const content = req.body.content;

  // Retrouve l'article et l'enregistre dans la base de données
  // Utilisation de la méthode save() qui retourne une promesse
  Post.findById(postId)
  .then(post => {
    if (!post) {
      const error = new Error('L\'article n\'existe pas.');
      error.statusCode = 404;
      throw error;
    }
 
    post.title = title;
    post.content = content;
    return post.save();
  })
  .then(result => {
    res.status(200).json(result);

  })
  .catch(err => {
    next(err);
  });
};

// Suprime un article grâce à son id
exports.deletePost = (req, res, next) => {
  const postId = req.params.postId;
  console.log('postId', postId)
  Post.findByIdAndRemove(postId)
  .then(() => {
    res.status(204).json();
  });
  
};


